﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARyzhova_Assignment1
{
    class Program
    {
        static void Main(string[] args)
        {
            bool isWaitingForRadius = true;
            bool isShowingMenu = true;
            Circle circle = new Circle();
            int radius = 0;
            int userInput = 0;
            int newRadius;

            Console.Write("Please enter a radius: ");

            while (isWaitingForRadius)
            {
                try
                {
                    radius = int.Parse(Console.ReadLine());
                }
                catch (Exception)
                {
                    Console.Write("Please enter an integer: ");
                    Console.ReadLine();
                }

                if (radius <= 0)
                {
                    Console.WriteLine("Radius should be greater 0");
                }
                else
                {
                    circle = new Circle(radius);
                    isWaitingForRadius = false;
                }
            }


            do
            {
                Console.WriteLine("Choose the following options:");
                Console.WriteLine("1. Get circle radius");
                Console.WriteLine("2. Change circle radius");
                Console.WriteLine("3. Get circle circumference");
                Console.WriteLine("4. Get circle area");
                Console.WriteLine("5. Exit");

                try
                {
                    userInput = int.Parse(Console.ReadLine());

                }
                catch (Exception)
                {
                    Console.Write("Please enter an integer: ");
                }


                switch (userInput)
                {
                    case 1:
                        Console.WriteLine($"The circle radius is: {circle.GetRadius()}");
                        break;
                    case 2:
                        Console.Write("Enter new radius:");
                        bool askAgain = true;
                        do
                        {
                            try
                            {
                                newRadius = int.Parse(Console.ReadLine());

                                if (newRadius <= 0)
                                {
                                    Console.WriteLine("Please enter a positive number");
                                }
                                else
                                {
                                    circle.SetRadius(newRadius);

                                    Console.WriteLine("Radius changed. New radius is " + circle.GetRadius());
                                    askAgain = false;
                                }
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Please enter an integer");
                            }
                        } while (askAgain);

                        break;
                    case 3:
                        Console.WriteLine($"Circle circumference is {circle.GetCircumference()}");
                        break;
                    case 4:
                        Console.WriteLine($"Circle area is {circle.GetArea()}");
                        break;
                    case 5:
                        isShowingMenu = false;
                        break;
                    default:
                        Console.WriteLine("Please enter a number from 1 to 5");
                        break;
                }
            } while (isShowingMenu);

        }
    }
}
