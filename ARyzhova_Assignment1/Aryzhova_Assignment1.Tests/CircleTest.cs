﻿using ARyzhova_Assignment1;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aryzhova_Assignment1.Tests
{
    [TestFixture]
    public class CircleTest
    {
        [Test]
        public void GetRadius_NotReturningZero()
        {
            Circle circle = new Circle();

            int radius = circle.GetRadius();

            Assert.NotZero(radius);
        }

        [Test]
        public void SetRadius_RadiusChanged()
        {
            Circle circle = new Circle(3);

            int oldRadius = circle.GetRadius();

            circle.SetRadius(5);

            int newRadius = circle.GetRadius();

            Assert.AreNotEqual(oldRadius, newRadius);
        }

        [Test]
        public void GetCircumference_CorrectValue()
        {
            Circle circle = new Circle(3);

            double myCircumference = 2 * 3 * Math.PI;

            double testCircumference = circle.GetCircumference();

            Assert.AreEqual(myCircumference, testCircumference);
        }

        [Test]
        public void GetArea_CorrectArea()
        {
            Circle circle = new Circle(3);

            double myArea = Math.PI * 3 * 3;

            double testArea = circle.GetArea();

            Assert.AreEqual(myArea, testArea);
        }
    }
}
